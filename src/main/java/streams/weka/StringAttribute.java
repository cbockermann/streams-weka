/**
 * 
 */
package streams.weka;

import java.util.ArrayList;

import weka.core.Attribute;

/**
 * @author chris
 * 
 */
public class StringAttribute extends Attribute {

	/** The unique class ID */
	private static final long serialVersionUID = -6478741288493392903L;

	/**
	 * @param attributeName
	 * @param attributeValues
	 */
	public StringAttribute(String attributeName) {
		super(attributeName, new ArrayList<String>());
		this.m_Type = Attribute.STRING;
	}

	public StringAttribute(String name, String firstValue) {
		this(name);
		this.addStringValue(firstValue);
	}
}