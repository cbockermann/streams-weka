/**
 * 
 */
package streams.weka;


/**
 * @author chris
 * 
 */
public class WekaRandomModel extends WekaModel {

	/** The unique class ID */
	private static final long serialVersionUID = -7836474790199937697L;

	/**
	 * @param wekaClassifier
	 */
	public WekaRandomModel() {
		super(new RandomGuess());
	}
}