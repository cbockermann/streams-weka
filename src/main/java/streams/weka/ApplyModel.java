/**
 * 
 */
package streams.weka;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;
import stream.io.SourceURL;
import weka.core.Attribute;
import weka.core.Instances;

/**
 * @author chris
 * 
 */
public class ApplyModel extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(ApplyModel.class);
	final ArrayList<Attribute> attributes = new ArrayList<Attribute>();
	final ArrayList<String> classes = new ArrayList<String>();
	Attribute label;
	Instances dataset;

	SourceURL model;
	WekaModel classifier;

	WekaModelService modelProvider;

	public ApplyModel() {
		classes.add("gamma");
		classes.add("proton");
	}

	/**
	 * @see stream.AbstractProcessor#init(stream.ProcessContext)
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);

		if (modelProvider == null && model == null) {
			throw new Exception("No model file provided!");
		}

		if (modelProvider == null) {
			log.info("Reading classifier from {}", model);
			InputStream fis = model.openStream();
			classifier = WekaModel.read(fis);
			fis.close();
			log.info("+------------------- WekaModel Provided ------------------");
			Map<String, String> info = classifier.info();
			for (String key : info.keySet()) {
				log.info("|   {} : {}", key, info.get(key));
			}
			log.info("+---------------------------------------------------------");
		} else {
			log.info("Fetching model from modelProvider '{}':", modelProvider);
			WekaModel model = modelProvider.getModel();
			if (model != null) {
				log.info("+------------------- WekaModel Loaded -------------------");
				Map<String, String> info = classifier.info();
				for (String key : info.keySet()) {
					log.info("|   {} : {}", key, info.get(key));
				}
				log.info("+--------------------------------------------------------");
			} else {
				log.info("No model provided, yet.");
			}
		}
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		if (classifier == null) {

			if (modelProvider != null) {
				// log.warn("Probing modelProvider for model...");
				classifier = modelProvider.getModel();

				if (classifier != null) {
					log.info("Retrieved model from '{}':", modelProvider);
					for (String line : classifier.toInfoString().split("\n")) {
						log.info(line);
					}
				}
			}
		}

		if (classifier == null) {
			log.warn("No model available, yet.");
			return input;
		}

		try {
			Data result = classifier.apply(input);
			return result;
		} catch (Exception e) {
			log.error("Failed to apply model: {}", e.getMessage());
			e.printStackTrace();
			System.exit(-1);
			throw new RuntimeException("Failed to apply model: "
					+ e.getMessage());
		}
	}

	/**
	 * @return the output
	 */
	public SourceURL getModel() {
		return model;
	}

	/**
	 * @param output
	 *            the output to set
	 */
	public void setModel(SourceURL output) {
		this.model = output;
	}

	/**
	 * @return the modelProvider
	 */
	public WekaModelService getModelProvider() {
		return modelProvider;
	}

	/**
	 * @param modelProvider
	 *            the modelProvider to set
	 */
	public void setModelProvider(WekaModelService modelProvider) {
		this.modelProvider = modelProvider;
	}
}