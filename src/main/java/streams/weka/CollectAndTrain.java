/**
 * 
 */
package streams.weka;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import stream.AbstractProcessor;
import stream.Configurable;
import stream.Data;
import stream.annotations.Parameter;
import stream.runtime.setup.ParameterInjection;
import stream.runtime.setup.factory.ObjectFactory;
import stream.util.Variables;
import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;

/**
 * # Collect-And-Train -- Building Models from streams
 * 
 * The `CollectAndTrain` processor collects data items from a stream until a
 * specified limit is reached. It then will build a classification model using
 * the collected items as example set. As soon as the model has been created it
 * is made available via the WekaModelService service provided by this
 * processor.
 * 
 * After the model has been built (i.e. after the specified number of examples
 * has been seen), the processor simply returns all further items as is and does
 * not do any retraining of the model.
 * 
 * 
 * ## Features for Training
 * 
 * The `features` parameter allows for the specification of attributes that
 * should be used for training of the classifier. Any key, which start with an
 * `@` character are automatically ignored as regular features and are not used
 * for learning. The special `@label` key is automatically expected to contain
 * the true class label of the examples if no other key was specified using the
 * `label` parameter.
 * 
 * 
 * ## Specifying the Classifier
 * 
 * The type of classifier trained by this processor is determined by the
 * `classifierType` parameter. By default, the processor will build a
 * *RandomForest* model.
 * 
 * Any parameters used by the classifier (e.g. the `numTrees` parameter of the
 * RandomForest model) can be specified as parameters of this processor and are
 * automatically delegated to the classifier.
 * 
 * @author Christian Bockermann
 * 
 */
public class CollectAndTrain extends AbstractProcessor implements Configurable,
		WekaModelService {

	static Logger log = LoggerFactory.getLogger(CollectAndTrain.class);

	@Parameter(description = "The list of features to be used to build the model. The label is automatically included in this list.", required = false, defaultValue = "*")
	String[] features = new String[] { "*" };

	@Parameter(description = "The attribute key holding the class label for training.", required = false, defaultValue = "@label")
	String label = "@label";

	@Parameter(description = "The number of examples that will be collected for training the classifier.", defaultValue = "1000000")
	int numberOfExamples = 100000;

	@Parameter(description = "The type of classifier which will be build using the collected training examples.", defaultValue = "weka.classifiers.trees.RandomTree")
	String classifierType = "weka.classifier.tree.RandomForest";

	@Parameter(description = "An output file where to store the model as soon as it has been build.", required = false)
	File output;

	final List<String> attributeNames = new ArrayList<String>();
	final Map<String, List<String>> nominalValues = new HashMap<String, List<String>>();

	/* This array is used to collect the raw training data. */
	double[][] instances;

	/*
	 * The pointer to the current element in the training data (used during
	 * collection)
	 */
	int currentIndex = 0;

	int maxIndex = 0;
	Long count = 0L;

	Classifier wekaClassifier;

	WekaModel model;

	public CollectAndTrain() {
		List<String> classLabels = new ArrayList<String>();
		nominalValues.put("@label", classLabels);
	}

	protected CollectAndTrain(String defaultType) {
		this();
		classifierType = defaultType;
	}

	protected Classifier createClassifier() {
		return wekaClassifier;
	}

	/**
	 * Checks if the given key is regarded as nominal attribute.
	 * 
	 * @param key
	 *            The key for which to check for a nominal mapping.
	 * @return <code>true</code>, if the given key has a nominal mapping of
	 *         values associated with it.
	 */
	protected boolean isNominal(String key) {
		return nominalValues.containsKey(key);
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		if (count > this.numberOfExamples) {

			if (this.model == null) {
				try {
					this.trainModel();
				} catch (Exception e) {
					log.error("Failed to train model: {}", e.getMessage());
				}
			}

			log.debug("{} examples already used for training - training completed.");
			return input;
		}

		if (!input.containsKey(label)) {
			log.warn(
					"item does not provide a label '{}' -- skipping item for training.",
					label);
			return input;
		}

		if (attributeNames.isEmpty()) {

			// Add the label attribute to the list of attribute names.
			//
			attributeNames.add(label);
			Serializable cls = input.get(label);
			if (cls instanceof Number) {
				log.info(
						"Found numeric class label '{}' in first item, value = {}",
						label, cls);
			} else {
				log.info("Found non-numeric label '{}'", label);
				List<String> values = nominalValues.get(label);
				if (values == null) {
					values = new ArrayList<String>();
					nominalValues.put(label, values);
				}
				log.info("   nominal value mapping: {}", values);
				if (!values.contains(cls.toString())) {
					values.add(cls.toString());
					nominalValues.put(label, values);
					log.info("First nominal class-value is {}", cls);
				}
			}

			// complete the list of attribute names and determine their type.
			//
			for (String key : input.keySet()) {

				if (attributeNames.contains(key)) {
					continue;
				}

				if (key.startsWith("@") && !key.equals(label)) {
					continue;
				}

				Serializable value = input.get(key);
				if (value instanceof Number) {
					attributeNames.add(key);
					continue;
				}

				if (!attributeNames.contains(key)) {
					attributeNames.add(key);
				}

				if (!nominalValues.containsKey(key)) {
					nominalValues.put(key, new ArrayList<String>());
				}

				log.warn(
						"Unsupported feature type '{}' in key '{}'  (skipping)",
						value.getClass().getCanonicalName(), key);
			}

			log.info("Created feature list based on first item: {}",
					attributeNames);

			instances = new double[numberOfExamples][attributeNames.size()];
		}

		// double[] row = new double[attributeNames.size()];
		int rowIndex = currentIndex % numberOfExamples;

		for (int featIndex = 0; featIndex < attributeNames.size(); featIndex++) {

			String feature = attributeNames.get(featIndex);
			Serializable value = input.get(feature);

			if (value instanceof Number) {
				// row[featIndex] = ((Number) value).doubleValue();
				instances[rowIndex][featIndex] = ((Number) value).doubleValue(); // idx.doubleValue();
			} else {
				if (nominalValues.containsKey(feature)) {
					String strValue = value.toString();
					List<String> vals = nominalValues.get(feature);
					Integer idx = vals.indexOf(strValue);
					if (idx < 0) {
						log.info("Adding new value {} for nominal feature {}",
								strValue, feature);
						vals.add(strValue);
						idx = vals.indexOf(strValue);
					}
					// row[featIndex] = idx.doubleValue();
					instances[rowIndex][featIndex] = idx.doubleValue();
				} else {
					log.warn(
							"No mapping exists for nominal value '{}' in attribute '{}'",
							value, feature);
				}
			}

		}

		currentIndex++;
		count++;

		return input;
	}

	protected void trainModel() throws Exception {

		final ArrayList<Attribute> attributes = new ArrayList<Attribute>();
		Attribute labelAttribute = null;

		for (int i = 0; i < attributeNames.size(); i++) {
			Attribute attribute;
			String feature = attributeNames.get(i);

			if (isNominal(feature)) {
				List<String> values = nominalValues.get(feature);
				attribute = new Attribute(feature, values);
			} else {
				attribute = new Attribute(feature);
			}

			if (this.label.equals(feature)) {
				labelAttribute = attribute;
			}
			attributes.add(attribute);
		}

		int size = Math.min(count.intValue(), numberOfExamples);
		log.info(
				"Building classifier using training dataset with {} samples and {} features.",
				size, attributes.size());

		Instances data = new Instances("Training Dataset", attributes, size);
		for (int i = 0; i < size; i++) {
			data.add(new DenseInstance(1.0d, instances[i]));
		}

		log.info("Using {} as class label.", labelAttribute);
		data.setClass(labelAttribute);

		Classifier classifier = createClassifier();
		log.info("Training classifier of type '{}'", classifier.getClass()
				.getCanonicalName());
		long start = System.currentTimeMillis();
		WekaModel wm = new WekaModel(classifier);
		for (String key : this.nominalValues.keySet()) {
			wm.setNominalValues(key, nominalValues.get(key));
		}
		wm.train(data);
		// forest.buildClassifier(dataset);
		long end = System.currentTimeMillis();
		log.info("Training took {} ms.", (end - start));

		log.info("Classifier trained:");
		for (String line : wm.toInfoString().split("\n")) {
			log.info(line);
		}

		model = wm;
	}

	/**
	 * @see stream.AbstractProcessor#finish()
	 */
	@Override
	public void finish() throws Exception {
		super.finish();

		if (model == null) {
			trainModel();
		}

		if (output != null && model != null) {
			log.info("Writing model to {}", output);
			OutputStream os;
			if (output.getName().endsWith(".gz")) {
				os = new GZIPOutputStream(new FileOutputStream(output));
			} else {
				os = new FileOutputStream(output);
			}
			synchronized (model) {
				WekaModel.write(model, os);
			}
			os.close();
		} else {
			log.debug("Parameter 'output' not set, not writing model.");
		}
	}

	/**
	 * @return the output
	 */
	public File getOutput() {
		return output;
	}

	/**
	 * @param output
	 *            the output to set
	 */
	public void setOutput(File output) {
		this.output = output;
	}

	/**
	 * @param document
	 *            If null, this object is initialized without a DOM element
	 *            specification (respective setters should be called on this
	 *            object beforehand!)
	 * @see stream.Configurable#configure(org.w3c.dom.Element)
	 */
	@Override
	public void configure(Element document) {
		
		if (document != null)
			log.debug("Configuring element {}", document.getNodeName());

		ObjectFactory factory = ObjectFactory.newInstance();
		Map<String, String> parameters = (document != null) ? factory.getAttributes(document) : null;

		String type = classifierType;
		if (parameters != null && parameters.get("classifier") != null) {
			type = parameters.get("classifier");
		}
		
		this.configure(type, parameters);
		
	}
	
	/**
	 * Configure this object based on classifier type and parameters.
	 * 
	 * @param type
	 *            The weka class to use
	 * @param parameters
	 *            The parameters to inject into the Weka object. May be null.
	 */
	public void configure(String type, Map<String, String> parameters) {
		
		try {
			log.debug("Creating new element of class '{}'", type);
			@SuppressWarnings("unchecked")
			Class<Classifier> clazz = (Class<Classifier>) Class.forName(type);
			wekaClassifier = clazz.newInstance();
			log.info("New classifier is: {}", wekaClassifier);
			
			if (parameters != null) {
				log.debug("Injecting parameters into classifier: {}", parameters);
				ParameterInjection.inject(wekaClassifier, parameters,
						new Variables());
			}

			log.debug("Classifier ready to be trained ;-)");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Failed to set up classifier of type '{}'", "");
			throw new RuntimeException("Failed to set up classifier!");
		}
		
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the features
	 */
	public String[] getFeatures() {
		return features;
	}

	/**
	 * @param features
	 *            the features to set
	 */
	public void setFeatures(String[] features) {
		this.features = features;
	}

	/**
	 * @return the classifierType
	 */
	public String getClassifier() {
		return classifierType;
	}

	/**
	 * @param classifierType
	 *            the classifierType to set
	 */
	public void setClassifier(String classifierType) {
		this.classifierType = classifierType;
	}

	/**
	 * @return the classifierType
	 */
	public String getClassifierType() {
		return classifierType;
	}

	/**
	 * @param classifierType
	 *            the classifierType to set
	 */
	public void setClassifierType(String classifierType) {
		this.classifierType = classifierType;
	}

	/**
	 * @return the numberOfExamples
	 */
	public int getNumberOfExamples() {
		return numberOfExamples;
	}

	/**
	 * @param numberOfExamples
	 *            the numberOfExamples to set
	 */
	public void setNumberOfExamples(int numberOfExamples) {
		this.numberOfExamples = numberOfExamples;
	}

	/**
	 * @see stream.service.Service#reset()
	 */
	@Override
	public void reset() throws Exception {
	}

	/**
	 * @see streams.weka.WekaModelService#getModel()
	 */
	@Override
	public WekaModel getModel() {
		return model;
	}
}