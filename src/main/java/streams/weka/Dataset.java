/**
 * 
 */
package streams.weka;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.Keys;
import stream.io.Stream;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.ProtectedProperties;

/**
 * @author Christian Bockermann
 *
 */
public class Dataset {

	static Logger log = LoggerFactory.getLogger(Dataset.class);

	final List<Data> items = new ArrayList<Data>();
	final Set<String> attributes = new LinkedHashSet<String>();
	Instances instances;

	public Dataset(Collection<Data> items) {
		this(items, null);
	}

	public Dataset(Collection<Data> items, Set<String> features) {
		log.debug("Creating dataset from {} items", items.size());
		ArrayList<Attribute> attributes = new ArrayList<Attribute>();

		Attribute classAttribute = null;
		Keys keys = new Keys("*");
		Iterator<Data> it = items.iterator();
		Data item = it.next();
		for (String key : keys.select(item.keySet())) {

			if (features != null && !features.contains(key)) {
				log.debug("skipping feature '{}'", key);
				continue;
			}

			if (key.startsWith("@")) {
				if (key.equals("@label")) {
					List<String> vals = new ArrayList<String>();
					vals.add("gamma");
					vals.add("proton");
					log.debug("Adding nominal attribute '{}' with values {}", key, vals);
					Attribute nom = new Attribute(key, vals, new ProtectedProperties(new Properties()));
					attributes.add(nom);
					classAttribute = nom;
					this.attributes.add(key);
				}
				continue;
			}

			Serializable value = item.get(key);
			if (value instanceof Number) {
				log.debug("Adding new numeric attribute {}", key);
				Attribute a = new Attribute(key);
				attributes.add(a);
				this.attributes.add(key);
				continue;
			}

			log.debug("Adding new string attribute {}", key);
			List<String> vals = new ArrayList<String>();
			vals.add(value.toString());
			Attribute a = new Attribute(key, (List<String>) null, new ProtectedProperties(new Properties()));
			attributes.add(a);
		}

		Instances instances = new Instances("DataSet", attributes, 1000);
		instances.setClass(classAttribute);

		while (item != null && it.hasNext()) {
			Instance instance = WekaUtils.createInstance(attributes, item);
			instances.add(instance);
			this.items.add(item);
			item = it.next();
		}

		this.instances = instances;
	}

	public Instances instances() {
		return instances;
	}

	public int size() {
		return items.size();
	}

	public Set<String> features() {
		return Collections.unmodifiableSet(this.attributes);
	}

	public static Instances readInstance(Stream stream, Keys keys) throws Exception {
		return readInstances(stream, keys, null);
	}

	public static Instances readInstances(Stream stream, Keys keys, String label) throws Exception {
		log.debug("Reading instances from {}, selector: {}", keys.toString(), stream);
		ArrayList<Attribute> attributes = new ArrayList<Attribute>();

		List<Attribute> classAttributes = new ArrayList<Attribute>();
		List<String> specialNames = new ArrayList<String>();

		Data item = stream.read();
		for (String key : keys.select(item.keySet())) {

			Serializable value = item.get(key);
			if (value instanceof Number) {
				log.debug("Adding new numeric attribute {}", key);
				Attribute a = new Attribute(key);
				if (key.startsWith("@")) {
					specialNames.add(key);
					classAttributes.add(a);
				} else {
					attributes.add(a);
				}

				continue;
			}

			log.debug("Adding new string attribute {}", key);
			List<String> vals = new ArrayList<String>();
			vals.add(value.toString());
			Attribute a = new Attribute(key, (List<String>) null, new ProtectedProperties(new Properties()));
			if (key.startsWith("@")) {
				specialNames.add(key);
				classAttributes.add(a);
			} else {
				attributes.add(a);
			}
		}

		log.info("Found {} regular attributes, {} special attributes", attributes.size(), specialNames.size());
		Attribute labelAttribute = null;

		if (classAttributes.size() > 0) {
			labelAttribute = classAttributes.get(0);

			if (label != null && specialNames.contains(label)) {
				labelAttribute = classAttributes.get(specialNames.indexOf(label));
			}

			if (classAttributes.size() > 1) {
				log.info("Multiple special attributes found: {}", specialNames);
			}

		}
		attributes.add(labelAttribute);

		Instances instances = new Instances("DataSet[" + stream.getId() + "]", attributes, 1000);

		log.info("Using class attribute {}", classAttributes.get(0).name());
		instances.setClass(classAttributes.get(0));

		while (item != null) {
			Instance instance = WekaUtils.createInstance(attributes, item);
			instances.add(instance);
			item = stream.read();
		}

		DecimalFormat nf = new DecimalFormat("##,###,###,###,###");
		log.debug("Read {} instances.", nf.format(instances.size()));
		return instances;
	}
}
