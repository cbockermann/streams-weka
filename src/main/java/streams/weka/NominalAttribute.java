/**
 * 
 */
package streams.weka;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import weka.core.Attribute;

/**
 * @author chris
 * @deprecated Tested this but does not work out - all members of Attribute are
 *             private :-(
 */
public class NominalAttribute extends Attribute {

	/** The unique class ID */
	private static final long serialVersionUID = -6478741288493392903L;

	final Map<String, Integer> nominalValues = new HashMap<String, Integer>();

	/**
	 * @param attributeName
	 * @param attributeValues
	 */
	public NominalAttribute(String attributeName) {
		super(attributeName, new ArrayList<String>());
	}

	public void addNominalValue(String value) {
		if (nominalValues.containsKey(value)) {
			return;
		}

		Integer idx = nominalValues.size() + 1;
		nominalValues.put(value, idx);
	}

}