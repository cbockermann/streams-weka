/**
 * 
 */
package streams.weka;

import stream.service.Service;

/**
 * @author chris
 * 
 */
public interface WekaModelService extends Service {

	public WekaModel getModel();
}
