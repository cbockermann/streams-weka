import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 */

/**
 * @author chris
 * 
 */
public class QFactorExploration {

	static Logger log = LoggerFactory.getLogger(QFactorExploration.class);

	public static void main(String[] args) throws Exception {

		ArrayList<DPoint> plot = new ArrayList<DPoint>();

		for (Double tpr = 0.0; tpr <= 1.0; tpr += 0.01) {
			for (Double fpr = 0.0; fpr <= 1.0; fpr += 0.01) {

				DPoint p = new DPoint(tpr, fpr);
				if (!Double.isNaN(p.q())) {
					plot.add(p);
				}
				// log.info("{} = {}", p, q);
			}
		}

		Collections.sort(plot);
		for (DPoint p : plot) {
			log.info("{} = {}", p, p.q());
		}
	}

	public static class DPoint implements Comparable<DPoint> {
		final double x;
		final double y;
		final Double q;

		public DPoint(double x, double y) {
			this.x = x;
			this.y = y;
			if (x > 0.0 && y != 0.0) {
				q = x / Math.sqrt(y);
			} else {
				q = Double.NaN;
			}
		}

		public double q() {
			return q;
		}

		public String toString() {
			DecimalFormatSymbols dfs = new DecimalFormatSymbols();
			dfs.setDecimalSeparator('.');
			DecimalFormat fmt = new DecimalFormat("0.0", dfs);
			return "(" + fmt.format(x) + "," + fmt.format(y) + ")";
		}

		/**
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		@Override
		public int compareTo(DPoint o) {

			if (this == o) {
				return 0;
			}

			Double oq = o.q;
			int r = q.compareTo(oq);
			if (r == 0) {
				r = (new Double(x)).compareTo(new Double(o.x));
				if (r == 0) {
					r = new Double(y).compareTo(new Double(o.y));
				}
				return r;
			} else {
				return r;
			}
		}
	}
}
