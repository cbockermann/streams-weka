/**
 * 
 */
package streams.weka;


/**
 * @author chris
 * 
 */
public class TrainRandomForest extends Train {

	public TrainRandomForest() {
		super(weka.classifiers.trees.RandomTree.class.getCanonicalName());
	}
}