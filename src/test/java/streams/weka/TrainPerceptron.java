/**
 * 
 */
package streams.weka;

/**
 * @author chris
 * 
 */
public class TrainPerceptron extends Train {

	public TrainPerceptron() {
		super(weka.classifiers.functions.VotedPerceptron.class
				.getCanonicalName());
	}
}